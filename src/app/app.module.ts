import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddressListModule } from '../modules/address-list/address-list.module';
import { AppRoutingModule } from './app-routing.module';
import { AddressDetailsModule } from '../modules/address-details/address-details.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AddressListModule,
    AppRoutingModule,
    HttpClientModule,
    AddressDetailsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
