import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AddressListComponent } from 'src/modules/address-list/address-list.component';
import { AddressDetailsComponent } from 'src/modules/address-details/address-details.component';

const routes: Routes = [
  {
    path: 'addresses',
    component: AddressListComponent
  },
  {
    path: 'addresses/:id',
    component: AddressDetailsComponent,
  },
  { path: '**',
    redirectTo: '/addresses',
    pathMatch: 'full'
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
