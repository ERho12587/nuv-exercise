import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressListComponent } from './address-list.component';
import { MatTableModule } from '@angular/material/table';


@NgModule({
  declarations: [AddressListComponent],
  imports: [
    CommonModule,
    MatTableModule,
  ],
  exports: [AddressListComponent],
})
export class AddressListModule { }
