import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Address } from 'src/interfaces/Address';
import { ApiList } from 'src/interfaces/ApiList';
import { AddressService } from 'src/services/address.service';

@Component({
  selector: 'address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.css']
})
export class AddressListComponent implements OnInit {
  public displayedColumns: string[] = ['name'];
  public dataSource: any;
  public dataLoaded: boolean = false;

  constructor(
    private addressService: AddressService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.addressService.getAddressData().subscribe((result: ApiList) => {
      this.dataSource = result;
      this.dataLoaded = true;
    });
  }

  navToDetail(target: Address) {
    this.addressService.selectedAddress = target;
    this.router.navigate([`${target.id.value}`], { relativeTo: this.route});
  }

}
