import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AddressService } from 'src/services/address.service';
import { Address } from '../../interfaces/Address';

@Component({
  selector: 'app-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.css']
})
export class AddressDetailsComponent implements OnInit {
  public address: Address | undefined;

  constructor(
    private addressService: AddressService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.address = this.addressService.selectedAddress;
    if (!this.address) {
      this.router.navigate(['/addresses']);
    }
  }

  navToList() {
    this.router.navigate(['/addresses']);
  }

}
