import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Address } from 'src/interfaces/Address';
import { ApiList } from 'src/interfaces/ApiList';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
public addresses: Array<Address> | undefined;
public selectedAddress: Address | undefined;

  constructor(private http: HttpClient) { }

  public getAddressData(): Observable<any> {
    return this.http.get('https://randomuser.me/api/?results=50&seed=48602412ecedbcf7');
  }
}
