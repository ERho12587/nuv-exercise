import { Address } from '../interfaces/Address';


export interface ApiList {
  page: number;
  results: Array<Address>;
  seed: string;
  version: string;
}
