export interface Address {
  cell: string;
  dob: {
    date: string;
    age: number;
  }
  email: string;
  gender: string;
  id: {
    name: string;
    value: string;
  }
  location: {
    city: string;
    coordinates: {
      latitude: string;
      longitude: string;
    }
    postcode: string;
    state: string;
    street: string;
    timezone: {
      offset: string;
      description: string;
    }
  }
  login: {
    md5: string;
    password: string;
    salt: string;
    sha256: string;
    shal: string;
    uuid: string;
    username: string;
  }
  nat: string;
  name: {
    first: string;
    last: string;
    title: string;
  }
  phone: string;
  picture: {
    large: string;
    medium: string;
    thumbnail: string;
  }
  registered: {
    age: number;
    date: string;
  }
}
