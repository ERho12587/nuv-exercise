# Setup
- Clone Repository
- Run `npm install`
- Run `npm start`
- In your browser, nagivate to `localhost:4200` (by default)

# Summary of Project
- Goal: Great a list/table of users/people where the user of the application is able to click and view additional details of the selected user.
- Approach:
  - After review of requirements, I decided to setup the app with the assumption that additional features could/would be added with nice-to-haves.
  - I created two child modules/components (AddressList & AddressDetails) in a modules directory, each with their own corresponding routes (/addresses & /addresses/{id}).
  - I used the Angular Material component library as a quick way to have a nicely formatted/themed table for use with the list of names and created a service that will handle API calls as well as setting the selected/viewed user/address.
  - Mostly from time contraints, it didn't seem that the RandomUsers API supported getting users by ID or SEED so I used the service to store the target user/address and then pass it to the detail page.
  - This detail page includes only the `full name` and `cell phone number`.
  - The detail page also has a back button to take the user back to the list page.

- Implemented Features
  - Table of users generated from RandomUsers API
  - Detail Page with user `first name`, `last name` and `cell phone number`.
  - Back button to take user back to list page.
  - Route "protection" in that only `/addresses` and `/addresses/:id` are viable. All other urls will route back to `/addresses`.

- Additional/Nice-To-Have Features
  - Table with additional information since there isn't much additional info in the detail page.
    - Estimate: 15m (not including discussion with product owner about additional fields, assuming this would be a real application)
  - Depending on if this were a real app, show additional user data in the details page.
    - Estimate: 2h
  - Pipe for phone numbers
    - Some difficulty here since this would need to account for commonly used phone number formatting for different countries
    - Estimate: 1d
  - Further implementation of Material Theme, fonts and general theming for entire app.
    - Estimate: 3d
  - Assuming this is a product for use, discuss with product owner on need for authentication, true route guards, update to the API to query for `users` by `ID`.
    - 1 sprint
  - Pagination to reduce total size of table.
    - 1d
